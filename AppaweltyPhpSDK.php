<?php
/**
 * @since		Dec 15th 2016
 * @author		clement@awelty.com
 * @version		1.0.0
 * @uses		PHP cURL
 * 
 * Helper for AppAwelty service usage (https://fr.appawelty.com)
 * API calls : http://api.appawelty.com
 * Documentation URL : http://doc.appawelty.com
 * 
 */
 
	// Modify these constants with your own API credentials
	define('APPAWELTY_API_PUBLIC_KEY',	'YOUR_API_PUBLIC_KEY');		// Your API Public Key
	define('APPAWELTY_API_PRIVATE_KEY',	'YOUR_API_PRIVATE_KEY');	// Your API Private Key
	define('APPAWELTY_API_EMAIL_ADDR',	'your@account-email.com');	// Your account e-mail address
	define('APPAWELTY_API_USER_AGENT',	'Your Application Name');	// Your application name (will be sent to API)
	
	
	// API Root URL (should not be changed)
	define('APPAWELTY_API_ROOT_URL', 	'http://api.appawelty.com');

	
	class AppaweltyPhpSDK
	{
		
		// Object attributes
		private $curl_handler;
		private $datetime;
		private $uri;
		private $verb;
		private $signature;
		private $payload;
		private $message;
		private $content_type;
		private $post_fields;
		private $curl_fields;
		private $output_json;
		private $output_object;
		
		
		/** ---------------------------------------------------------------------------------------
		* @since	2 mai 2016
		* @method	__construct
		* @param	void
		* @desc		Class constructor
		* ---------------------------------------------------------------------------------------- */
		public function __construct()
		{
			// Error message if cURL unavaibale
			if (!function_exists('curl_init')) { exit('Can\'t run AppaweltyPhpSDK : current implementation requires PHP cURL.'); }
				
			// Initializing cURL handler
			$this->curl_handler = curl_init();
				
			// Default cURL Options
			curl_setopt_array($this->curl_handler, array(
									CURLOPT_USERAGENT	=> APPAWELTY_API_USER_AGENT,
									CURLOPT_RETURNTRANSFER	=> TRUE,
									CURLOPT_BINARYTRANSFER 	=> TRUE,
									CURLOPT_FORBID_REUSE	=> TRUE,
									CURLOPT_FOLLOWLOCATION 	=> TRUE,
									CURLOPT_SSL_VERIFYPEER 	=> FALSE
			));
				
			// Sets dateTime for API requests
			$this->datetime = round(microtime(TRUE)*1000);
		}
		
		
		/** ---------------------------------------------------------------------------------------
		 * @since	2 mai 2016
		 * @method	__destruct
		 * @param	void
		 * @desc		Class desctructor
		 * ---------------------------------------------------------------------------------------- */
		public function __destruct()
		{
			if (!is_null($this->curl_handler)) { curl_close($this->curl_handler); }
		}
		
		
		/** ---------------------------------------------------------------------------------------
		 * @since	24 mai 2016
		 * @method	getInstance
		 * @param	void
		 * @desc	Returns current object instance
		 * ---------------------------------------------------------------------------------------- */
		public function getInstance()
		{
			return $this;
		}
		
		
		/** ---------------------------------------------------------------------------------------
		 * @since	24 mai 2016
		 * @method	apiQuery
		 * @param	verb : GET/POST/HEAD/PUT/DELETE
		 * @param	uri : URI requested
		 * @desc	Exectutes a request on API and returns JSON result
		 * ---------------------------------------------------------------------------------------- */
		private function apiQuery($verb = 'GET', $uri = '')
		{
			if (!$uri) { return false; }
			else { $this->uri = $uri; }
			
			$this->verb = $verb;
			
			// Preprends slash if not specified in request URI
			if (!preg_match('/^\//Ui', $this->uri)) { $this->uri = '/'.$this->uri; }
			
			// Sets messages to sign
			$this->message = $this->verb.$this->uri.$this->datetime;
		
			// Generates HMAC signature
			$this->signature = hash_hmac('sha1', $this->message, APPAWELTY_API_PRIVATE_KEY);
	
			// Sets additional cURL headers
			$this->curl_fields = array(
					CURLOPT_HTTPHEADER => array(
								'X-Public-Key:'.APPAWELTY_API_PUBLIC_KEY,
								'X-Datetime:'.$this->datetime,
								'X-Signature:'.$this->signature,
								'email_addr:'.APPAWELTY_API_EMAIL_ADDR)
					);
			
			// If request is a POST
			if ($this->verb == 'POST')
			{
				$this->curl_fields[CURLOPT_POST] = TRUE;
			}
				
			// If request is a DELETE or PUT
			if ($this->verb == 'DELETE' || $this->verb == 'PUT' || $this->verb == 'GET' || $this->verb == 'HEAD')
			{
				$this->curl_fields[CURLOPT_CUSTOMREQUEST] = $this->verb;
			}
				
			// Sets cURL option array
			curl_setopt_array($this->curl_handler, $this->curl_fields);
			
			// API URL called
			$url = APPAWELTY_API_ROOT_URL.$this->uri;
			
			// Replaces potential spaces in URI
			curl_setopt($this->curl_handler, CURLOPT_URL, str_replace(' ', '%20', $url));
			
			// Executes cURL request
			$this->output_json = curl_exec($this->curl_handler);
		
			// Decodes the JSON output (php object)
			$this->output_object = json_decode($this->output_json);
			
			// Returns the resulting PHP object
			return $this->output_object;
		}
		

		/** ---------------------------------------------------------------------------------------
		 * @since	15 déc. 2016
		 * @method	setRequestLimit
		 * @param	o : offset
		 * @param	l : limit
		 * @desc		Sets the offset/limit parameters for an API request
		 * ---------------------------------------------------------------------------------------- */
		private function setRequestLimit($o = '', $l = '')
		{
			$o = strval($o);
			$l = strval($l);
			if (!$o) { $o = '0'; }
			return ($o || $l) ? '/'.$o.','.$l : '';
		}
		
		
		/** ---------------------------------------------------------------------------------------
		* @since	24 mai 2016
		* @method	getCategories
		* @param	void
		* @desc		Returns Categories set on current application
		* ---------------------------------------------------------------------------------------- */
		public function getCategories()
		{
			return $this->apiQuery('GET', 'get/categories');
		}


		/** ---------------------------------------------------------------------------------------
		 * @since	16 déc. 2016
		 * @method	getAudiences
		 * @param	void
		 * @desc	Returns audiences set in the current application
		 * ---------------------------------------------------------------------------------------- */
		public function getAudiences()
		{
			return $this->apiQuery('GET', 'get/audiences');
		}
		
		
		/** ---------------------------------------------------------------------------------------------------
		* @since	15 déc. 2016
		* @method	isCategoryVisible
		* @param	category_id : Category ID from getCategories()
		* @desc		Checks and returns if a category is visible (should be displayed or hidden to end users)
		* ---------------------------------------------------------------------------------------------------- */
		public function isCategoryVisible($category_id = '')
		{
			if (!$category_id) return false;
			return $this->apiQuery('GET', 'get/category_visible/'.$category_id);
		}
		
		
		/** ---------------------------------------------------------------------------------------
		* @since	15 déc. 2016
		* @method	getEventsByCategory
		* @param	category_id : Category ID from getCategories()
		* @param	(optional) offset : Query offset (paged listing)
		* @param	(optional) limit : Query limit (paged listing)
		* @desc		Returns event listing from specified category
		* ---------------------------------------------------------------------------------------- */
		public function getEventsByCategory($category_id = '', $offset = '0', $limit = '')
		{
			if (!$category_id) return false;
			$appends = $this->setRequestLimit($offset, $limit);
			return $this->apiQuery('GET', 'get/events/'.$category_id.$appends);
		}
		
		
		/** ---------------------------------------------------------------------------------------
		* @since	15 déc. 2016
		* @method	getEvent
		* @param	event_id : Event ID
		* @desc		Returns all the information on a specific event (from its ID)
		* ---------------------------------------------------------------------------------------- */
		public function getEvent($event_id = '')
		{
			if (!$event_id) return false;
			
			return $this->apiQuery('GET', 'get/event/'.$event_id);
		}
		
		
		/** ---------------------------------------------------------------------------------------
		* @since	15 déc. 2016
		* @method	getEventsByCoordinates
		* @param	latitude : GPS latitude
		* @param	longitude : GPS longitude
		* @param	(optional) radius : in kilometers, max radius for your events request
		* @param	(optional) categories : "all" or array of category IDs
		* @param	(optional) offset : Query offset (paged listing)
		* @param	(optional) limit : Query limit (paged listing)
		* @desc		Returns an event listing from GPS coordinates (latitude, longitude) and a radius
		* ---------------------------------------------------------------------------------------- */
		public function getEventsByCoordinates($latitude = '', $longitude = '', $radius = '20', $categories = array(), $offset = '0', $limit = '')
		{
			if (!$latitude || !$longitude) return false;

			$appends = (is_array($categories)) ? '/'.implode(',', $categories) : '/all';
			$appends .= $this->setRequestLimit($offset, $limit);

			return $this->apiQuery('GET', 'geo/'.$latitude.','.$longitude.'/'.$radius.$appends);
		}
		
		
		/** ---------------------------------------------------------------------------------------
		* @since	15 déc. 2016
		* @method	getEventsByCity
		* @param	city_id : City ID (obtained from city manipulation requests)
		* @param	(optional) categories : "all" or array of category IDs
		* @param	(optional) offset : Query offset (paged listing)
		* @param	(optional) limit : Query limit (paged listing)
		* @desc		Returns an event listing from a specitif City
		* ---------------------------------------------------------------------------------------- */
		public function getEventsByCity($city_id = '', $categories = array(), $offset = '0', $limit = '')
		{
			if (!$city_id) return false;

			$appends = (is_array($categories)) ? '/'.implode(',', $categories) : '/all';
			$appends .= $appends .= $this->setRequestLimit($offset, $limit);
			
			return $this->apiQuery('GET', 'get/city/'.$city_id.$appends);
		}
		
		
		/** ---------------------------------------------------------------------------------------
		* @since	15 déc. 2016
		* @method	getHighlights
		* @param	(optional) offset : Query offset (paged listing)
		* @param	(optional) limit : Query limit (paged listing)	
		* @desc		Returns highlighted events in the application
		* ---------------------------------------------------------------------------------------- */
		public function getHighlights($offset = '0', $limit = '')
		{
			$appends = $this->setRequestLimit($offset, $limit);
			return $this->apiQuery('GET', 'get/highlights'.$appends);
		}
		
		
		/** ---------------------------------------------------------------------------------------
		* @since	15 déc. 2016
		* @method	getNextEvents
		* @param	(optional) offset : Query offset (paged listing)
		* @param	(optional) limit : Query limit (paged listing)	
		* @desc		Returns very next events happening
		* ---------------------------------------------------------------------------------------- */
		public function getNextEvents($offset = '0', $limit = '')
		{
			$appends = $this->setRequestLimit($offset, $limit);
			return $this->apiQuery('GET', 'get/next'.$appends);
		}
		
		
		/** ---------------------------------------------------------------------------------------
		 * @since	15 déc. 2016
		 * @method	getRecentEvents
		 * @param	(optional) offset : Query offset (paged listing)
		 * @param	(optional) limit : Query limit (paged listing)
		 * @desc	Returns the last events added in the application
		 * ---------------------------------------------------------------------------------------- */
		public function getRecentEvents($offset = '0', $limit = '')
		{
			$appends = $this->setRequestLimit($offset, $limit);
			return $this->apiQuery('GET', 'get/recent'.$appends);
		}
		
		
		/** ---------------------------------------------------------------------------------------
		 * @since	15 déc. 2016
		 * @method	getPopularEvents
		 * @param	(optional) offset : Query offset (paged listing)
		 * @param	(optional) limit : Query limit (paged listing)
		 * @desc	Returns the most popular events (most added to users's favorites)
		 * ---------------------------------------------------------------------------------------- */
		public function getPopularEvents($offset = '0', $limit = '')
		{
			$appends = $this->setRequestLimit($offset, $limit);
			return $this->apiQuery('GET', 'get/popular'.$appends);
		}
		
		
		/** ----------------------------------------------------------------------------------------------
		* @since	16 déc. 2016
		* @method	searchEvents
		* @param	category_id : event category ID
		* @param	(optional) timestamp_from : Timestamp starting, events from this date only
		* @param	(optional) paying_or_free : "all", "free", or "paying" : all, free or paid events only
		* @param	(optional) audience_id : Limiting to an audience only, by its ID
		* @desc		Returns an event listing corresponding specific criteria (search)
		* ----------------------------------------------------------------------------------------------- */
		public function searchEvents($category_id = 'all', $timestamp_from = '', $paying_or_free = 'all', $audience_id = 'all', $offset = '0', $limit = '')
		{
			if (!$timestamp_from) return false;
			
			$category_id = !$category_id ? 'all' : $category_id;
			$paying_or_free = !$paying_or_free ? 'all' : $paying_or_free;
			$audience_id = !$audience_id ? 'all' : $audience_id;
			
			$appends = '/'.$category_id.'/'.$timestamp_from.'/'.$paying_or_free.'/'.$audience_id;
			$appends .= $this->setRequestLimit($offset, $limit);
			return $this->apiQuery('GET', 'search'.$appends);
		}
		
		
		/** ----------------------------------------------------------------------------------------------------
		* @since	16 déc. 2016
		* @method	getUpdates
		* @param	timestamp_from : For events updated since this date
		* @param	(optional) category_id_or_offset : Category ID or offset and Limit, depending on the request
		* @desc		Returns all events updated (or deleted, or status changed) since a specific date
		* ----------------------------------------------------------------------------------------------------- */
		public function getUpdates($timestamp_from = '', $category_id_or_offset = '0', $limit = '')
		{
			if (!$timestamp_from) return false;
			
			if ($limit)
			{
				// Search through all for updates, no category specified
				$appends = $this->setRequestLimit($category_id_or_offset, $limit);
				return $this->apiQuery('GET', 'get/updates/'.$timestamp_from.$appends);
			}
			else
			{
				// Search through a specific category for updates
				return $this->apiQuery('GET', 'get/updates/'.$timestamp_from.'/'.$category_id_or_offset);
			}
		}
		
		
		/** ---------------------------------------------------------------------------------------
		* @since	16 déc. 2016
		* @method	getCitiesByCoordinates
		* @param	latitude : GPS latitude
		* @param	longitude : GPS longitude
		* @param	(optional) radius : Max radius for the search
		* @param	(optional) limit : Limiting the results
		* @desc		Returns a list of cities in the system, based on the GPS point and a radius
		* ---------------------------------------------------------------------------------------- */
		public function getCitiesByCoordinates($latitude = '', $longitude = '', $radius = '', $limit = '')
		{
			if (!$latitude || !$longitude) return false;
			
			// Can't specify a limit without a radius argument
			if ($limit && !$radius) return false;
			
			$appends = !$radius ? '' : '/'.$radius;
			$appends .= !$limit ? '' : '/'.$limit;
			
			return $this->apiQuery('GET', 'geocities/'.$latitude.','.$longitude.$appends);
		}
		
		
		/** --------------------------------------------------------------------------------------------------------------
		* @since	16 déc. 2016
		* @method	getCitiesByName
		* @param	search_terms : for example "par" for Paris, what the user could have typed
		* @param	(optional) limit : Limiting the results
		* @desc		Returns a lit of cities based on a typed search, like a form field with autocomplete, or search bar
		* --------------------------------------------------------------------------------------------------------------- */
		public function getCitiesByName($search_terms = '', $limit = '')
		{
			if (!$search_terms) return false;
			$appends = !$limit ? '' : '/'.$limit;
			
			// Transforms the search terms to be URI-compliant
			$search_terms = preg_replace('/[éèêëÉÈËÊ]+/U', 'e', strtolower($search_terms));
			$search_terms = preg_replace('/[àâäÂÄÀ]+/U', 'a', $search_terms);
			$search_terms = preg_replace('/[îïÏÎ]+/U', 'i', $search_terms);
			$search_terms = preg_replace('/[ôöÖÔ]+/U', 'o', $search_terms);
			$search_terms = preg_replace('/[ùüûÙÜÛ]+/U', 'u', $search_terms);
			$search_terms = preg_replace('/[^a-zA-Z0-9 -]+/U', '-', $search_terms);
			
			return $this->apiQuery('GET', 'cityfinder/'.$search_terms.$appends);
		}
		
	}
		