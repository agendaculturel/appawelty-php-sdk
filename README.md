# AppAwelty PHP SDK #

![fr.png](https://bitbucket.org/repo/zExe5j/images/3458491297-fr.png) http://fr.appawelty.com

### API Documentation (for developers) : ###

![fr.png](https://bitbucket.org/repo/zExe5j/images/3458491297-fr.png) http://doc.appawelty.com

# SDK Usage #

__Installation__ :
		
```
#!php
<?php
require_once('/path/to/class/AppaweltyPhpSDK.php');
$a = new AppaweltyPhpSDK();

```

Make sure you replace the dummy API credentials (these constants are at the top of the class) by your own API access you received : private key, public key, account e-mail address, and application name.

__Requirements__ :

Current SDK's implementation requires [PHP cURL](http://php.net/manual/en/book.curl.php) installed on your server.

__Additional information__ :

The requests return PHP objects (from parsed JSON) you can directly use in your PHP application.

If you want to get JSON directly (so without the *json_decode()* execution after an API request) and not PHP objetcs, you can simply remove the instruction *json_decode()* in the body of method *apiQuery()* (around line 153).

#### Query examples : ####

Get categories from your application :


```
#!php
<?php
$categories = $a->getCategories();
```

Returning events from category ID 63 :


```
#!php
<?php
$events = $a->getEventsFromCategory(63);
```

There are many other methods you can use in the SDK. Search cities, events, load listings with many options, get event detail, etc.

All of the methods you can use are available and deeply detailed in the [online documentation](http://doc.appawelty.com).

####Visit our other projects : ####

- [AgendaCulturel.fr](https://www.agendaculturel.fr)
- [E-monsite.com](https://www.e-monsite.com)
- [Luminjo.com](https://fr.luminjo.com)
